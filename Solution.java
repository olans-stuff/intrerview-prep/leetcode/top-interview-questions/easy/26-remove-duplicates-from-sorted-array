class Solution {
    public int removeDuplicates(int[] nums) {
        // so need to run trhough the array, check each value and if more than one exists,
        // K = count of unique elements, so k can be count of non _ elements
        int k = 1; // as first element will be unique anyways
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;

            }
        }
        return k;
    }
}